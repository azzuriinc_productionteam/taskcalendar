import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import webpack from 'webpack'

const src  = path.resolve(__dirname, 'src')
const dist = path.resolve(__dirname, 'public/dist')

export default {
  entry: ['babel-polyfill', src + '/index.js'],
  output: {
    path: dist,
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ]
  },
  resolve: {
    extensions: ['.js'],
  },
  plugins: [
    ...(
      (process.env.NODE_ENV === 'production') ? [
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            warnings: false,
          },
        }),
      ] : []
    ),
  ],
  // devtool: "#inline-source-map",
  watchOptions: {
    aggregateTimeout: 300,
    ignored: /node_modules/
  },
}
