import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as CommonAction from '../actions/CommonAction.js';
import * as CommonConstant from '../constants/CommonConstant.js';

import Top from './Top/Top';

export class Root extends Component {
  constructor(props) {
    super(props);
    // 画面のスクロールを抑止
    document.addEventListener('touchmove', function(e){ e.preventDefault(); }, false);
  }

  componentDidCatch(error, info) {
    console.log("------ error occurred !! ------");
    console.log(error.message);
    console.log(error.stack);
    console.log(info.stack);

    document.getElementById('debug').className = '';
  }

  render() {
    var main = null;
    switch(this.props.common.page) {
      case CommonConstant.TOP:
        main = <Top />
        break;
      default:
        main = (<div>
            <h1>ERROR!!</h1>
            <h1>{this.props.common.page}</h1>
          </div>);
        break;
    }

    return (
      <div>
        {main}
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    common: state.common,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commonAction: bindActionCreators(CommonAction, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root);
