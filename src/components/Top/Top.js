import moment from 'moment';

import Script from 'react-load-script';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import $ from 'jquery';
import 'fullcalendar';
import 'fullcalendar-scheduler';
import 'fullcalendar/dist/gcal';
import qtip from 'qtip2';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as CommonConstant from '../../constants/CommonConstant';
import * as CommonAction from '../../actions/CommonAction';

import Loading from './Loading';
import AddButton from './AddButton';
import InputModal from './InputModal';
import UpdateModal from './UpdateModal';
import FilterModal from './FilterModal';

export class Top extends Component {
  constructor(props) {
    super(props);
    this.rendered = false;
    this.timelineMonthRendered = false;

    this.scrollPos = {
      'timelineDay': 0,
      'timelineThreeDays': [0, 0],
      'timelineMonth': [0, 0],
    };

    this.beforeTitle = {
      'timelineDay': "",
      'timelineThreeDays': "",
      'timelineMonth': "",
    };

    this.state = {
      view: 'timelineDay',
      shrink: false,
    }
  }

  init() {
    this.props.commonAction.init();
  }

  showRegistModal() {
    this.props.commonAction.showRegistModal();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.common.initDone) {
      console.log("event refetch");
      console.log("view: " + this.state.view);
      // $("calendar").fullCalendar('refetchEvents');

      console.log(JSON.stringify(this.props.common.filterList));
      const events = [].concat((this.state.view === 'timelineMonth') ? this.props.common.eventListMonth : this.props.common.eventList).filter((val) => {
        console.log(val.creator);
        return this.props.common.filterList[val.creator];
      });
      const workerListIndexes = this.props.common.workerList.reduce((acc, cur) => {
        acc[cur.address] = cur.kana;
        return acc;
      }, {});
      const resources = (this.state.view === 'timelineMonth') ? [].concat(this.props.common.resourcesMonth).filter((val) => {
        return this.props.common.filterList[val.id];
      }) : this.props.common.resources;
      resources.sort((a, b) => {
        let a_kana = '';
        let b_kana = '';
        if (this.state.view === 'timelineMonth') {
          a_kana = workerListIndexes[a.id];
          b_kana = workerListIndexes[b.id];
        } else {
          a_kana = workerListIndexes[a.creator];
          b_kana = workerListIndexes[b.creator];
        }
        if (a_kana < b_kana) {
          return -1;
        } else if(a_kana > b_kana) {
          return 1;
        } else {
          return 0;
        }
      });

      console.log(`resource: ${JSON.stringify(resources)}`);
      console.log(`events: ${JSON.stringify(events)}`);

      if (this.rendered) {
        $('#calendar').fullCalendar('destroy');
      }

      const numOfNotWorkingDay = calcNumOfNotWorkingDay();
      $('#calendar').fullCalendar({
        weekends: false,
        height: window.innerHeight - 100,
        scrollTime: '00:00',
        customButtons: {
          shrink: {
            text: 'shrink',
            click: () => {
              this.setState({shrink: true});
            }
          },
          normal: {
            text: 'normal',
            click: () => {
              this.setState({shrink: false});
            }
          },
          filter: {
            text: 'filter',
            click: () => {
              this.props.commonAction.showFilterModal();
            }
          }
        },
        header: {
          left: `today prev,next ${((!this.state.shrink) ? 'shrink' : 'normal')}  filter`,
          center: 'title',
          right: 'timelineDay,timelineThreeDays,timelineMonth'
        },
        defaultView: this.state.view,
        views: {
          timelineDay: {
            type: 'timeline',
            duration: { days: 1 },
            resourceGroupField: 'staff',
            resourceLabelText: 'イベント',
          },
          timelineThreeDays: {
            type: 'timeline',
            duration: { days: 3 },
            resourceGroupField: 'staff',
            resourceLabelText: 'イベント',
            slotLabelFormat: [
              'M/D ddd', // top level of text
              'H:mm'        // lower level of text
            ],
          },
          timelineMonth: {
            type: 'timeline',
            duration: { days: 31 },
            resourceAreaWidth: "100px",
            resourceLabelText: '担当者',
            slotLabelInterval: '02:00',
            slotWidth: 20,
            slotLabelFormat: [
              'M/D ddd', // top level of text
              'H'        // lower level of text
            ],
            minTime: '9:00:00',
            maxTime: '21:00:00',
            filterResourcesWithEvents: false,
            // scrollTime: `${Number(moment().format('D')) - numOfNotWorkingDay}.00:00:00`,
          }
        },
        minTime: '9:00:00',
        maxTime: '20:00:00',
        events,
        resources,
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        filterResourcesWithEvents: true,
        nowIndicator: true,
        editable: true,
        eventRender: (event,element,view) => {
          if (view.name === 'timelineMonth') {
            // console.log(`eventColor: ${event.eventColor}`);
            element.qtip({
              content:{
                // text:`<h3>${event.title}</h3>`,
                text:`<h4>${moment(event.start).format('HH:mm')} 〜 ${moment(event.end).format('HH:mm')}</h4><br/><h3>${event.title}</h3>`,
              },
              position: {
                my: 'top left',
                at: 'bottom left',
              },
              style: {
                classes: 'qtip-light'
              },
              show: 'mouseover',
              hide: {
                when: {
                  event: 'mouseout unfocus',
                }
              }
            });

            element.css({"background-color": event.eventColor, "border-color": event.eventColor});
          }
        },
        eventDrop: (event, delta ) => {
          this.props.commonAction.updateEvent({
            id: event.orgId,
            title: event.title,
            start: event.start,
            end: event.end,
            description: event.description,
          });
        },
        eventResize: (event, delta) => {
          this.props.commonAction.updateEvent({
            id: event.orgId,
            title: event.title,
            start: event.start,
            end: event.end,
            description: event.description,
          });
        },
        viewRender: (view, element) => {
          // console.log(`viewRender: ${this.lastView}`);
          if (this.lastView && this.lastView !== view.name) {
            this.lastView = "";
            this.setState({view: view.name});
          }
          this.lastView = view.name;
        },
        eventClick: (calEvent, jsEvent, view) => {
          if (calEvent.calendarId !== CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID) {
            return;
          }
          if (view.name === 'timelineMonth') {
            $('.qtip').remove();
          }
          const event = {
            id: calEvent.orgId,
            title: calEvent.title,
            start: calEvent.start,
            end: calEvent.end,
            description: calEvent.description,
          }
          this.props.commonAction.showUpdateModal(event);
        },
        eventAfterAllRender: ( view ) => {
          if (this.state.shrink !== prevState.shrink || this.state.view !== prevState.view) {
            if (this.state.shrink) {
              $('#additionalStyle').attr('href', './css/fullcalendar.add.shrink.css');
              $('.fc-time-area > div > div > div > div.fc-content > div > table > tbody > tr > td > div > div.fc-event-container > a.fc-timeline-event').each(function() {
                if($(this).css("top") !== "0px") {
                  console.log($(this).css("top"));
                  $(this).css("top", "0px");
                }
              });
              $('.fc-time-area > div > div > div > div.fc-content > div > table > tbody > tr > td > div > div.fc-event-container > a.fc-timeline-event').each(function() {
                console.log($(this).css("top"));
              });
            } else {
              $('#additionalStyle').attr('href', './css/fullcalendar.add.noshrink.css');
              $('.fc-time-area > div > div > div > div.fc-content > div > table > tbody > tr').each(function() {
                const resourceId = $(this).data("resource-id");
                const height = $(this).find(".fc-event-container").outerHeight();

                $(`.fc-resource-area.fc-widget-content > div > div > div > div.fc-content > div > table > tbody > tr[data-resource-id = '${resourceId}'] > td > div`).css("height", height);
              });
            }
            if (this.state.view === prevState.view) {
              this.scrollPos[view.name] = 0;
            }
          }

          // スクロール位置の復元
          const scrollArea = $(`#calendar > div.fc-view-container > div > table > tbody > tr > td.fc-time-area.fc-widget-content > div > div`);
          const scrollElm = scrollArea.children('.fc-scroller-canvas');
          const scrollTop = (view.title !== this.beforeTitle[view.name]) ? 0 : (view.name !== "timelineDay") ? this.scrollPos[view.name][0] : this.scrollPos[view.name];
          let scrollLeft = 0;
          console.log(`name: ${view.name}`);
          if (view.name === "timelineMonth") {
            if (!this.timelineMonthRendered) {
              const selector = `.fc-widget-header[data-date="${moment().format("YYYY-MM-DD")}T09:00:00"]`;
              // const selector = `.fc-widget-header[data-date="2018-08-31T09:00:00"]`;
              console.log(`selector: ${selector}`);
              console.log(`${JSON.stringify($(selector).offset())}`);
              console.log(`${JSON.stringify($(selector).parents("tbody").offset())}`);

              const todayLeft = $(selector).offset().left + ($(selector).innerWidth() / 2);
              const center = $(document).width() / 2;
              const diff = todayLeft - center;
              scrollLeft = (diff > 0) ? diff : 0;
              this.scrollPos[view.name][1] = scrollLeft;
            } else {
              scrollLeft = (view.title !== this.beforeTitle[view.name]) ? 0 : this.scrollPos[view.name][1];
            }
          } else if(view.name === "timelineThreeDays") {
            scrollLeft = (view.title !== this.beforeTitle[view.name]) ? 0 : this.scrollPos[view.name][1];
          }

          console.log(`scrollLeft: ${scrollLeft}`);

          scrollArea.scrollTop(scrollTop);
          scrollArea.scrollLeft(scrollLeft);

          this.beforeTitle[view.name] = view.title;

          scrollArea.on('scroll', (e) => {
            const height = $(e.currentTarget).children('.fc-scroller-canvas').innerHeight();
            const width = (view.name === "timelineMonth") ? $(e.currentTarget).children('.fc-scroller-canvas').innerWidth() : 0;

            if (view.name !== "timelineDay") {
              this.scrollPos[view.name] = [scrollArea.scrollTop(), scrollArea.scrollLeft()];
            } else {
              this.scrollPos[view.name] = scrollArea.scrollTop();
            }
          });

          if (!this.timelineMonthRendered && view.name === "timelineMonth") {
            this.timelineMonthRendered = true;
          }
        }
      });
      this.rendered = true;
    }
  }

  render() {
    const loading = (!this.props.common.initDone) ? <Loading /> : null;
    const addButton = (this.props.common.initDone) ? <AddButton onClick={() => {this.showRegistModal();}}/> : null;
    const registModal = (this.props.common.showRegistModal) ? <InputModal></InputModal> : null;
    const updateModal = (this.props.common.showUpdateModal) ? <UpdateModal event={this.props.common.updateTargetEvent}></UpdateModal> : null;
    const filterModal = (this.props.common.initDone && this.props.common.showFilterModal) ? <FilterModal event={this.props.common.doFilter}></FilterModal> : null;

    return (
      <Wrapper className="page top">
        {loading}
        <div id="calendar"></div>
        {addButton}
        <Script
          url={CommonConstant.GOOGLE_API_CLIENT_LIBRARY}
          onLoad={() => this.init()}>
        </Script>
        {registModal}
        {updateModal}
        {filterModal}
      </Wrapper>
    );
  }
}

function calcNumOfNotWorkingDay() {
  let retNum = 0;
  // 月初の曜日
  const firstDay = moment().date(1).day();
  if (firstDay === 0) {
    retNum += 1;
  }
  const firstSaturday = (6 - firstDay) + 1;

  retNum += Math.ceil((moment().date() - firstSaturday) / 7) * 2;
  if (moment().day() === 6) {
    retNum += 1;
  }

  return retNum;
}

const Wrapper = styled.div`
  margin: 50px;
  height: 100%;
`;

function mapStateToProps(state) {
  return {
    common: state.common,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commonAction: bindActionCreators(CommonAction, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Top);
