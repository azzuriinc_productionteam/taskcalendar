import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit * 2,
  },
  absolute: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
});

export class AddButton extends Component {
  render() {
    return (
      <Tooltip title="Add" placement="bottom-end">
        <Button variant="fab" color="secondary" style={{position: "absolute", right: "10px", bottom: "30px", zIndex: 2,}} onClick={this.props.onClick}>
          <AddIcon />
        </Button>
      </Tooltip>
    );
  }
}

export default withStyles(styles)(AddButton);
