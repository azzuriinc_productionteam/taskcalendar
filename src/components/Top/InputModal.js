import moment from 'moment';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';

import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import { DateTimePicker } from 'material-ui-pickers';

import Dropdown from './InputModal/Dropdown';

import * as CommonAction from '../../actions/CommonAction';
import * as CommonConstant from '../../constants/CommonConstant';

export class InputModal extends Component {
  constructor(props) {
    super(props);

    const nowMinute = moment().minute();
    const minute = (Math.ceil(nowMinute / 15) * 15) % 60;
    const hour = (minute === 0) ? moment().hour() + 1 : moment().hour();
    const now = moment().hour(hour).minute(minute);
    this.state = {
      title: '',
      startDate: now,
      endDate: moment(now).add(30, 'minutes'),
      staff: this.props.common.userId,
      memo: '',
      priority: 2,
    };
  }

  handleChange(key, value) {
    this.setState({
      [key]: value
    });
  };


  handleChangeStartDate(value) {
    console.log(value);
    console.log(JSON.stringify(this.state));

    this.setState({
      startDate: value,
      endDate: (value.isBefore(this.state.endDate)) ? this.state.endDate : value,
    });
  };

  registEvent() {
    console.log(JSON.stringify(this.state));
    this.props.commonAction.registEvent(this.state);
  }

  render() {
    const workerList = this.props.common.workerList.sort((a, b) => {
      if (a.kana < b.kana) {
        return -1;
      } else if(a.kana > b.kana) {
        return 1;
      } else {
        return 0;
      }
    }).map((obj) => {
      return {text: obj.name, value: obj.address}
    });

    return (
      <Wrapper>
        <InputArea>
          <InputRow>
            <TextField
              label="タイトル"
              placeholder="タイトル"
              fullWidth={true}
              onChange={(e)=>{this.handleChange('title', e.target.value)}}
            />
          </InputRow>
          <InputRow>
            <MuiPickersUtilsProvider
              utils={MomentUtils}>
              <Input>
                <DateTimePicker
                  autoOk
                  ampm={false}
                  value={this.state.startDate}
                  onChange={(date)=>{this.handleChangeStartDate(date)}}
                  label="開始日時"
                  showTodayButton={true}
                />
              </Input>
              <Input>
                <DateTimePicker
                  autoOk
                  disablePast
                  ampm={false}
                  value={this.state.endDate}
                  onChange={(date)=>{this.handleChange("endDate", date)}}
                  label="終了日時"
                  showTodayButton={true}
                />
              </Input>
            </MuiPickersUtilsProvider>
          </InputRow>
          <InputRow>
            <Input>
              <Dropdown
                label={'担当者'}
                value={this.props.common.userId}
                list = {workerList}
                onChange={(val)=>{this.handleChange('staff', val)}}/>
            </Input>
            <Input>
              <Dropdown
                label={'優先度'}
                value={2}
                list = {CommonConstant.PRIORITY_LIST}
                onChange={(val)=>{this.handleChange('priority', val)}}/>
            </Input>
          </InputRow>
          <InputRow>
            <TextField
              label="メモ"
              placeholder="メモ"
              fullWidth={true}
              multiline={true}
              rows={3}
              onChange={(e)=>{this.handleChange('memo', e.target.value)}}
            />
          </InputRow>
          <InputRow>
            <Input>
              <Button variant="contained" size="large" color="primary" style={{"marginTop": "30px",}} onClick={() => {this.registEvent()}}>
                登録
              </Button>
            </Input>
          </InputRow>
          <Button variant="fab" color="secondary" style={{position: "absolute", right: "-15px", top: "-20px"}} onClick={() => {this.props.commonAction.closeRegistModal()}}>
            <CloseIcon  style={{"fontSize": "50px"}}/>
          </Button>
        </InputArea>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0,0,0,0.5);
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
`;

const InputArea = styled.div`
  position: relative;
  width: 700px;
  height: 475px;
  background-color: #fff;
  border-radius: 10px;
  box-sizing: border-box;
  padding: 50px;
`;

const InputRow = styled.div`
  display: flex;
  width: 100%;
  margin-top: 20px;
  &:first-child {
    margin-top: 0px;
  }
`;

const Input = styled.div`
  margin-right: 50px;
  &:last-child {
    margin-right: 0px;
  }
`;


function mapStateToProps(state) {
  return {
    common: state.common,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commonAction: bindActionCreators(CommonAction, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InputModal);
