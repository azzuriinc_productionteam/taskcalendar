import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      value: '',
    };

    if (this.props.value) {
      this.state.value = this.props.value;
    }
  }

  handleChange(event) {
    this.props.onChange(event.target.value);
    this.setState({ value: event.target.value });
  };

  render() {
    const list = this.props.list.map((obj) => {
      return <MenuItem key={`menu_item_${obj.value}`} value={obj.value}>{obj.text}</MenuItem>
    });

    return (
      <Wrapper>
        <FormControl fullWidth={true}>
          <InputLabel>{this.props.label}</InputLabel>
          <Select
            value={this.state.value}
            onChange={(e) => {this.handleChange(e);}}
          >
            {list}
          </Select>
        </FormControl>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  width: 200px;
`;


export default withStyles(styles)(Dropdown);
