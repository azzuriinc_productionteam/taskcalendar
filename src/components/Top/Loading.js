import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as CommonAction from '../../actions/CommonAction';

export class Loading extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.state = {
      completed: 0,
    }
  }

  render() {
    return (
      <Wrapper>
        <Progress>
          <LinearProgress variant="determinate" value={this.props.common.loadingProgress} />
        </Progress>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
  padding-left: 10%;
  padding-right: 10%;
`;

const Progress = styled.div`
  flex-grow: 1;
`;

function mapStateToProps(state) {
  return {
    common: state.common,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commonAction: bindActionCreators(CommonAction, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Loading);
