import moment from 'moment';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';

import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import { DateTimePicker } from 'material-ui-pickers';

import Dropdown from './InputModal/Dropdown';

import * as CommonAction from '../../actions/CommonAction';
import * as CommonConstant from '../../constants/CommonConstant';

export class UpdateModal extends Component {
  constructor(props) {
    super(props);

    console.log(`updateModal: ${JSON.stringify(this.props.event)}`);

    const descriptionObj = (this.props.event.description) ? JSON.parse(this.props.event.description) : null;
    this.state = {
      id: this.props.event.id,
      title: this.props.event.title,
      start: moment(this.props.event.start),
      end: moment(this.props.event.end),
      staff: (descriptionObj) ? descriptionObj.staff : "",
      memo: (descriptionObj) ? descriptionObj.memo : "",
      priority: (descriptionObj && descriptionObj.priority) ? descriptionObj.priority : 2,
    };
  }

  handleChange(key, value) {
    this.setState({
      [key]: value
    });
  };


  handleChangeStartDate(value) {
    console.log(value);
    console.log(JSON.stringify(this.state));

    this.setState({
      start: value,
      end: (value.isBefore(this.state.end)) ? this.state.end : value,
    });
  };

  updateEvent() {
    console.log(JSON.stringify(this.state));
    const description = {
      staff : this.state.staff,
      memo: this.state.memo,
      priority: this.state.priority,
    }

    const eventObj = {
      id: this.state.id,
      title: this.state.title,
      start: this.state.start,
      end: this.state.end,
      description: JSON.stringify(description),
    }
    this.props.commonAction.updateEvent(eventObj);
  }

  deleteEvent() {
    this.props.commonAction.deleteEvent(this.state.id);
  }

  render() {
    const workerList = this.props.common.workerList.sort((a, b) => {
      if (a.kana < b.kana) {
        return -1;
      } else if(a.kana > b.kana) {
        return 1;
      } else {
        return 0;
      }
    }).map((obj) => {
      return {text: obj.name, value: obj.address}
    });

    return (
      <Wrapper>
        <InputArea>
          <InputRow>
            <TextField
              value={this.state.title}
              label="タイトル"
              placeholder="タイトル"
              fullWidth={true}
              onChange={(e)=>{this.handleChange('title', e.target.value)}}
            />
          </InputRow>
          <InputRow>
            <MuiPickersUtilsProvider
              utils={MomentUtils}>
              <Input>
                <DateTimePicker
                  autoOk
                  ampm={false}
                  value={this.state.start}
                  onChange={(date)=>{this.handleChangeStartDate(date)}}
                  label="開始日時"
                  showTodayButton={true}
                />
              </Input>
              <Input>
                <DateTimePicker
                  autoOk
                  disablePast
                  ampm={false}
                  value={this.state.end}
                  onChange={(date)=>{this.handleChange("end", date)}}
                  label="終了日時"
                  showTodayButton={true}
                />
              </Input>
            </MuiPickersUtilsProvider>
          </InputRow>
          <InputRow>
            <Input>
              <Dropdown
                label={'担当者'}
                value={this.state.staff}
                list = {workerList}
                onChange={(val)=>{this.handleChange('staff', val)}}/>
            </Input>
            <Input>
              <Dropdown
                label={'優先度'}
                value={this.state.priority}
                list = {CommonConstant.PRIORITY_LIST}
                onChange={(val)=>{this.handleChange('priority', val)}}/>
            </Input>
          </InputRow>
          <InputRow>
            <TextField
              value={this.state.memo}
              label="メモ"
              placeholder="メモ"
              fullWidth={true}
              multiline={true}
              rows={3}
              onChange={(e)=>{this.handleChange('memo', e.target.value)}}
            />
          </InputRow>
          <InputRow>
            <Input>
              <Button variant="contained" size="large" color="primary" style={{"marginTop": "30px",}} onClick={() => {this.updateEvent()}}>
                更新
              </Button>
            </Input>
            <Input>
              <Button variant="contained" size="large" color="secondary" style={{"marginTop": "30px",}} onClick={() => {this.deleteEvent()}}>
                削除
              </Button>
            </Input>
          </InputRow>
          <Button variant="fab" color="secondary" style={{position: "absolute", right: "-15px", top: "-20px"}} onClick={() => {this.props.commonAction.closeUpdateModal()}}>
            <CloseIcon  style={{"fontSize": "50px"}}/>
          </Button>
        </InputArea>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0,0,0,0.5);
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
`;

const InputArea = styled.div`
  position: relative;
  width: 700px;
  height: 475px;
  background-color: #fff;
  border-radius: 10px;
  box-sizing: border-box;
  padding: 50px;
`;

const InputRow = styled.div`
  display: flex;
  width: 100%;
  margin-top: 20px;
  &:first-child {
    margin-top: 0px;
  }
`;

const Input = styled.div`
  margin-right: 50px;
  &:last-child {
    margin-right: 0px;
  }
`;


function mapStateToProps(state) {
  return {
    common: state.common,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commonAction: bindActionCreators(CommonAction, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateModal);
