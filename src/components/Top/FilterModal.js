import moment from 'moment';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ReactIScroll from 'react-iscroll';
import IScroll from 'iscroll';

import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';

import * as CommonAction from '../../actions/CommonAction';
import * as CommonConstant from '../../constants/CommonConstant';

export class FilterModal extends Component {
  constructor(props) {
    super(props);
    this.iscrollOptions = {
      mouseWheel: true,
      scrollbars: true, /* スクロールバーを表示 */
      shrinkScrollbars: 'scale', /* スクロールバーを伸縮 */
    };

    this.directorList = this.props.common.workerList.filter(
      (obj) => {
        return (obj.director === 1)
      }
    ).map(
      (obj) => {
        return obj.address;
      }
    );

    this.creatorList = this.props.common.workerList.filter(
      (obj) => {
        return (obj.creator === 1)
      }
    ).map(
      (obj) => {
        return obj.address;
      }
    );

    this.checkerList = this.props.common.workerList.filter(
      (obj) => {
        return (obj.checker === 1)
      }
    ).map(
      (obj) => {
        return obj.address;
      }
    );

    const checked = Object.assign({}, this.props.common.filterList);
    const checkedCondition = {};
    const indeterminate = {};

    checkConditionState('director', checked, this.directorList, checkedCondition, indeterminate);
    checkConditionState('creator', checked, this.creatorList, checkedCondition, indeterminate);
    checkConditionState('checker', checked, this.checkerList, checkedCondition, indeterminate);

    this.state = {
      checked,
      checkedCondition,
      indeterminate,
    }
  }

  handleChange(val) {
    const newChecked = Object.assign({}, this.state.checked, {
      [val]: !this.state.checked[val],
    });

    const checkedCondition = {};
    const indeterminate = {};

    checkConditionState('director', newChecked, this.directorList, checkedCondition, indeterminate);
    checkConditionState('creator', newChecked, this.creatorList, checkedCondition, indeterminate);
    checkConditionState('checker', newChecked, this.checkerList, checkedCondition, indeterminate);

    this.setState({
      checked: newChecked,
      checkedCondition,
      indeterminate,
    })
  }

  handleChangeCondition(val) {
    const newCondition = Object.assign({}, this.state.checkedCondition, {
      [val]: (this.state.indeterminate[val]) ? true : !this.state.checkedCondition[val],
    });
    const newIndeterminate = Object.assign({}, this.state.indeterminate, {
      [val]: false,
    });

    console.log(`newCondition: ${JSON.stringify(newCondition)}`)

    const newChecked = Object.assign({}, this.state.checked);
    for(const key of Object.keys(newChecked)) {
      if (val === "director" && this.directorList.includes(key)) {
        newChecked[key] = newCondition.director;
      } else if(val === "creator" && this.creatorList.includes(key)) {
        newChecked[key] = newCondition.creator;
      } else if(val === "checker" && this.checkerList.includes(key)) {
        newChecked[key] = newCondition.checker;
      }
    }

    checkConditionState('director', newChecked, this.directorList, newCondition, newIndeterminate);
    checkConditionState('creator', newChecked, this.creatorList, newCondition, newIndeterminate);
    checkConditionState('checker', newChecked, this.checkerList, newCondition, newIndeterminate);

    this.setState({
      checked: newChecked,
      checkedCondition: newCondition,
      indeterminate: newIndeterminate,
    });
  }

  render() {
    const workerList = [].concat(this.props.common.workerList).sort(
      (a,b) => {
        if(a.kana < b.kana) {
          return -1;
        } else if(a.kana > b.kana) {
          return 1;
        } else {
          return 0;
        }
      }
    ).map((obj) => {
      return {text: obj.name, value: obj.address}
    })

    const list = [];
    for (const workerInfo of workerList) {
      list.push(<FormControlLabel
          key={`filterModal_${workerInfo.value}`}
          control={
            <Checkbox
              checked={this.state.checked[workerInfo.value]}
              onChange={() => {
                this.handleChange(workerInfo.value)
              }}
              value={workerInfo.value}
              color="primary"
            />
          }
          label={workerInfo.text}
          style={{width: "130px"}}
        />);
    }

    const opts = {
      director: { indeterminate: (this.state.indeterminate.director) ? true : false },
      creator: { indeterminate: (this.state.indeterminate.creator) ? true : false },
      checker: { indeterminate: (this.state.indeterminate.checker) ? true : false },
    };

    return (
      <Wrapper>
        <InputArea>
          <FormControlLabel
              key={`filterModal_conditions_director`}
              control={
                <Checkbox
                  checked={this.state.checkedCondition.director}
                  onChange={() => {this.handleChangeCondition('director')}}
                  color="primary"
                  {...opts.director}
                />
              }
              label='ディレクター'
              style={{marginLeft:"7px"}}
            />
          <FormControlLabel
              key={`filterModal_conditions_creator`}
              control={
                <Checkbox
                  checked={this.state.checkedCondition.creator}
                  onChange={() => {this.handleChangeCondition('creator')}}
                  color="primary"
                  {...opts.creator}
                />
              }
              label='制作'
            />
          <FormControlLabel
              key={`filterModal_conditions_checker`}
              control={
                <Checkbox
                  checked={this.state.checkedCondition.checker}
                  onChange={() => {this.handleChangeCondition('checker')}}
                  color="primary"
                  {...opts.checker}
                />
              }
              label='校正可能'
            />
          <CheckboxArea>
            <ReactIScroll
              iScroll={IScroll}
              options={this.iscrollOptions}>
              <CheckboxAreaInner>
                {list}
              </CheckboxAreaInner>
            </ReactIScroll>
          </CheckboxArea>
          <Button variant="contained" size="large" color="primary" style={{"marginTop": "30px",}} onClick={() => {this.props.commonAction.setFilter(this.state.checked)}}>
            設定
          </Button>
          <Button variant="fab" color="secondary" style={{position: "absolute", right: "-15px", top: "-20px"}} onClick={() => {this.props.commonAction.closeFilterModal()}}>
            <CloseIcon  style={{"fontSize": "50px"}}/>
          </Button>
        </InputArea>
      </Wrapper>
    );
  }
}

function checkConditionState(type, checkList, targetList, checkedCondition, indeterminate) {
  let numOfChecked = 0;
  for (const val of targetList) {
    if (checkList[val]) {
      numOfChecked ++;
    }
  }

  console.log(`numOfChecked: ${numOfChecked}`);

  if(numOfChecked === 0) {
    checkedCondition[type] = false;
    indeterminate[type] = false;
  } else if(numOfChecked < targetList.length) {
    checkedCondition[type] = true;
    indeterminate[type] = true;
  } else {
    checkedCondition[type] = true;
    indeterminate[type] = false;
  }
}

const Wrapper = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0,0,0,0.5);
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
`;

const InputArea = styled.div`
  position: relative;
  width: 700px;
  height: 80%;
  background-color: #fff;
  border-radius: 10px;
  box-sizing: border-box;
  padding: 50px;
`;

const CheckboxArea = styled.div`
  height: 70%;
  border: 1px dotted rgb(51,51,51);
  border-radius: 5px;
`;

const CheckboxAreaInner = styled.div`
  box-sizing: border-box;
  padding-left: 20px;
  padding-bottom: 20px;
`;


function mapStateToProps(state) {
  return {
    common: state.common,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commonAction: bindActionCreators(CommonAction, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterModal);
