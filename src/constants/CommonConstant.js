export const INIT = 'INIT';
export const CHANGE_VISIBLE_REGIST_MODAL = 'CHANGE_VISIBLE_REGIST_MODAL';
export const CHANGE_VISIBLE_UPDATE_MODAL = 'CHANGE_VISIBLE_UPDATE_MODAL';
export const CHANGE_VISIBLE_FILTER_MODAL = 'CHANGE_VISIBLE_FILTER_MODAL';
export const REGIST_EVENT = 'REGIST_EVENT';
export const UPDATE_EVENT = 'UPDATE_EVENT';
export const DELETE_EVENT = 'DELETE_EVENT';
export const SET_FILTER = 'SET_FILTER';

export const REDUCER_INIT = 'REDUCER_INIT';
export const REDUCER_CHANGE_VISIBLE_REGIST_MODAL = 'REDUCER_CHANGE_VISIBLE_REGIST_MODAL';
export const REDUCER_CHANGE_VISIBLE_UPDATE_MODAL = 'REDUCER_CHANGE_VISIBLE_UPDATE_MODAL';
export const REDUCER_CHANGE_VISIBLE_FILTER_MODAL = 'REDUCER_CHANGE_VISIBLE_FILTER_MODAL';
export const REDUCER_REGIST_EVENT = 'REDUCER_REGIST_EVENT';
export const REDUCER_UPDATE_EVENT = 'REDUCER_UPDATE_EVENT';
export const REDUCER_DELETE_EVENT = 'REDUCER_DELETE_EVENT';
export const REDUCER_PROGRESS_INCREMENT = 'REDUCER_PROGRESS_INCREMENT';
export const REDUCER_SET_FILTER = 'REDUCER_SET_FILTER';

export const PAGE_TOP = 'PAGE_TOP';

export const GOOGLE_API_CLIENT_ID = '764425432278-svgcqe6pjb5tgnr74v0dsfmmu7bh4jp2.apps.googleusercontent.com';
export const GOOGLE_API_SCOPES = 'https://www.googleapis.com/auth/calendar';

export const GOOGLE_RESOURCE_CALENDAR_ID = 'bigm2y.com_dku7g49eard7k8u7m48ol41mbs@group.calendar.google.com';

// export const WORKER_LIST = [
//   {name: '新本', address: 'niimoto@bigm2y.com', director: 1, creator: 0, checker: 0, kana: 'ニイモト'},
//   {name: '中薗', address: 'nakazono@bigm2y.com', director: 1, creator: 0, checker: 0, kana: 'ナカゾノ'},
//   {name: '芦野', address: 'ashino@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'アシノ'},
//   {name: '卓也', address: 'takuya@bigm2y.com', director: 1, creator: 0, checker: 0, kana: 'タクヤ'},
//   {name: '春山', address: 'haruyama@bigm2y.com', director: 1, creator: 0, checker: 0, kana: 'ハルヤマ'},
//   {name: '宮崎', address: 'miyazaki@bigm2y.com', director: 1, creator: 0, checker: 0, kana: 'ミヤザキ'},
//   {name: '針生', address: 'hariu@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'ハリウ'},
//   {name: '濱井', address: 'natsuki@bigm2y.com', director: 0, creator: 0, checker: 0, kana: 'ハマイ'},
//   {name: '生馬', address: 'ikuma@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'イクマ'},
//   {name: '杉田', address: 'sugita@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'スギタ'},
//   {name: '横山', address: 'yokoyama@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'ヨコヤマ'},
//   {name: '植松', address: 'uematsu@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'ウエマツ'},
//   {name: '高橋弘和', address: 'takahashi@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'タカハシヒロカズ'},
//   {name: '中川', address: 'nakagawa@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'ナカガワ'},
//   {name: 'キム', address: 'kim@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'キム'},
//   {name: '尾留川', address: 'birukawa@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'ビルカワ'},
//   {name: '中原', address: 'nakahara@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'ナカハラ'},
//   {name: '横内', address: 'yokouchi@bigm2y.com', director: 0, creator: 0, checker: 1, kana: 'ヨコウチ'},
//   {name: '木村', address: 'kimura@bigm2y.com', director: 0, creator: 1, checker: 0, kana: 'キムラ'},
//   {name: '下地', address: 'shimozi@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'シモヂ'},
//   {name: '柏木', address: 'kashiwagi@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'カシワギ'},
//   {name: '佐藤未優', address: 'miyu@bigm2y.com', director: 0, creator: 0, checker: 1, kana: 'サトウミユ'},
//   {name: '佐藤寛大', address: 'kandai@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'サトウカンダイ'},
//   {name: '羽鳥', address: 'hatori@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'ハトリ'},
//   {name: '青木', address: 'aoki@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'アオキ'},
//   {name: '坂田', address: 'k-sakata@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'サカタ'},
//   {name: '中島', address: 'nakajima@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'ナカジマ'},
//   {name: '高橋浩介', address: 'k-takahashi@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'タカハシコウスケ'},
//   {name: '大田', address: 'r-ota@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'オオタ'},
//   {name: '安藤', address: 'w-ando@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'アンドウ'},
//   {name: '伊東', address: 'itok@bigm2y.com', director: 0, creator: 0, checker: 1, kana: 'イトウ'},
//   {name: '上野', address: 'w-ueno@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'ウエノ'},
//   {name: '冨木', address: 'ｍ-tomiki@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'トミキ'},
//   {name: '森', address: 'h-mori@bigm2y.com', director: 1, creator: 0, checker: 1, kana: 'モリ'},
//   {name: '山際', address: 'm-yamagiwa@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'ヤマギワ'},
//   {name: '難波', address: 'k-nanba@bigm2y.com', director: 0, creator: 1, checker: 1, kana: 'ナンバ'},
// ];

export const PRIORITY_LIST = [
  {text: '高', value: 3},
  {text: '中', value: 2},
  {text: '低', value: 1},
];

export const GOOGLE_API_CLIENT_LIBRARY = 'https://apis.google.com/js/client.js';

export const GOOGLE_API_DISCOVERY_URL = 'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest';

export const LOCAL_STORAGE_TOKEN_KEY = 'AZZURI_TASK_CALENDAR_GOOGLE_AUTH_TOKEN';

export const COLOR_ORIGIN_CALENDAR = 'gray';
export const COLOR_RESOUCE_CALENDAR = ['', 'lightseagreen', 'mediumblue', 'crimson'];

export const HETEML_API_URL = 'https://deneb001.heteml.jp/taskcalendarAPPs';
export const HETEML_API_BASIC_AUTH_USERNAME = 'azzuri';
export const HETEML_API_BASIC_AUTH_PASSWORD = '3iwjtVMzjDLKlOPI1suGw1cHM1ND93B9';
export const HETEML_API_URL_USER = `${HETEML_API_URL}/users.json`;
