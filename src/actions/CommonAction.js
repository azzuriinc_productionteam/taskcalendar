import * as CommonConstant from '../constants/CommonConstant';

export function init() {
  return {
    type: CommonConstant.INIT,
  }
}

export function showRegistModal() {
  return {
    type: CommonConstant.CHANGE_VISIBLE_REGIST_MODAL,
    visible: true,
  }
}

export function closeRegistModal() {
  return {
    type: CommonConstant.CHANGE_VISIBLE_REGIST_MODAL,
    visible: false,
  }
}

export function registEvent(registInfo) {
  return {
    type: CommonConstant.REGIST_EVENT,
    registInfo,
  }
}

export function updateEvent(event) {
  return {
    type: CommonConstant.UPDATE_EVENT,
    event,
  }
}

export function deleteEvent(eventId) {
  return {
    type: CommonConstant.DELETE_EVENT,
    eventId,
  }
}

export function showUpdateModal(event) {
  return {
    type: CommonConstant.CHANGE_VISIBLE_UPDATE_MODAL,
    visible: true,
    event,
  }
}

export function closeUpdateModal() {
  return {
    type: CommonConstant.CHANGE_VISIBLE_UPDATE_MODAL,
    visible: false,
  }
}

export function showFilterModal() {
  return {
    type: CommonConstant.CHANGE_VISIBLE_FILTER_MODAL,
    visible: true,
  }
}

export function closeFilterModal() {
  return {
    type: CommonConstant.CHANGE_VISIBLE_FILTER_MODAL,
    visible: false,
  }
}

export function setFilter(list) {
  return {
    type: CommonConstant.SET_FILTER,
    list,
  }
}
