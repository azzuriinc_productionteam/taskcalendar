import moment from 'moment';
import * as CommonConstant from '../constants/CommonConstant';

const initialState = {
  page: CommonConstant.TOP,
  initDone: false,
  userId: '',
  eventList: [],
  eventListMonth: [],
  resources: [],
  resourcesMonth: [],
  filterList: [],
  showRegistModal: false,
  showUpdateModal: false,
  showFilterModal: false,
  loadingProgress: 0,
  updateTargetEvent: null,
  workerList: [],
};

export default function commonReducer(state = initialState, action) {
  console.log(`commonReducer: ${JSON.stringify(action)}`);
  switch (action.type) {
    case CommonConstant.REDUCER_INIT:
      return Object.assign({}, state, {
        initDone: true,
        userId: action.userId,
        eventList: action.eventList,
        eventListMonth: action.eventList.map((obj) => {
          return Object.assign({}, obj, {
            resourceId: obj.creator,
          })
        }),
        resources: action.eventList.map((obj) => {
          return {
            id: obj.id,
            creator: obj.creator,
            staff: obj.staff,
            title: obj.title,
            eventColor: obj.eventColor,
          }
        }),
        resourcesMonth: action.workerList.map((obj) => {
          return {
            id: obj.address,
            title: obj.name,
          }
        }),
        filterList: action.workerList.reduce((acc, currentVal) => {
          acc[currentVal.address] = true;
          return acc;
        }, {}),
        workerList: [].concat(action.workerList),
      });
    case CommonConstant.REDUCER_CHANGE_VISIBLE_REGIST_MODAL:
      return Object.assign({}, state, {
        showRegistModal: action.visible,
      });
    case CommonConstant.REDUCER_CHANGE_VISIBLE_UPDATE_MODAL:
      return Object.assign({}, state, {
        showUpdateModal: action.visible,
        updateTargetEvent: action.event,
      });
    case CommonConstant.REDUCER_CHANGE_VISIBLE_FILTER_MODAL:
      return Object.assign({}, state, {
        showFilterModal: action.visible,
      });
    case CommonConstant.REDUCER_REGIST_EVENT:
      const eventObj = Object.assign({}, action.result);

      return Object.assign({}, state, {
        eventList: [].concat(state.eventList).concat([eventObj]),
        eventListMonth: [].concat(state.eventListMonth).concat([
          Object.assign({}, eventObj, {
            resourceId: eventObj.creator,
          })
        ]),
        resources: [].concat(state.eventList).concat([{
          id: eventObj.id,
          creator: eventObj.creator,
          staff: eventObj.staff,
          title: eventObj.title,
          eventColor: eventObj.eventColor,
        }]),
        showRegistModal: false,
      });
    case CommonConstant.REDUCER_UPDATE_EVENT:
      const updateObj = Object.assign({}, action.result);
      let updateTarget = null;
      let targetIndex = 0;
      for (let target of state.eventList) {
        if (target.orgId === updateObj.orgId) {
          break;
        }
        targetIndex++;
      }
      const replaceObj = Object.assign({}, state.eventList[targetIndex], updateObj, {
        id: `${updateObj.orgId}${updateObj.creator}`,
        resourceId: `${updateObj.orgId}${updateObj.creator}`,
      });

      state.eventList.splice(targetIndex, 1, replaceObj);
      state.eventListMonth.splice(targetIndex, 1, Object.assign({}, replaceObj,  {resourceId: updateObj.creator}));

      return Object.assign({}, state, {
        eventList: [].concat(state.eventList),
        eventListMonth: [].concat(state.eventListMonth),
        resources: state.eventList.map((obj) => {
          return {
            id: obj.id,
            creator: obj.creator,
            staff: obj.staff,
            title: obj.title,
            eventColor: obj.eventColor,
          }
        }),
        showUpdateModal: false,
      });
    case CommonConstant.REDUCER_DELETE_EVENT:
      let deleteIndex = 0;
      for (let target of state.eventList) {
        if (target.orgId === action.eventId) {
          break;
        }
        deleteIndex++;
      }

      state.eventList.splice(deleteIndex, 1);
      state.eventListMonth.splice(deleteIndex, 1);

      return Object.assign({}, state, {
        eventList: [].concat(state.eventList),
        eventListMonth: [].concat(state.eventListMonth),
        resources: state.eventList.map((obj) => {
          return {
            id: obj.id,
            creator: obj.creator,
            title: obj.title,
            staff: obj.staff,
            eventColor: obj.eventColor,
          }
        }),
        showUpdateModal: false,
      });
    case CommonConstant.REDUCER_PROGRESS_INCREMENT:
      return Object.assign({}, state, {
        loadingProgress: action.progress,
      });
    case CommonConstant.REDUCER_SET_FILTER:
      return Object.assign({}, state, {
        filterList: action.list,
        showFilterModal: false,
      });
    default:
      return state;
  }
}
