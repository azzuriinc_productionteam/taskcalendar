import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux';

import Root from './components/Root';
import createFinalStore from './store';

const store = createFinalStore();

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Provider store={store}>
        <Root title="event_calendar">
        </Root>
      </Provider>
    )
  }
}

render(<App/>, document.getElementById('app'))
