import axios from 'axios';
import moment from 'moment';

import { delay } from 'redux-saga';
import { call, put, fork, take, takeEvery } from 'redux-saga/effects';

import * as CallGoogleApi from '../utils/CallGoogleApi';
import * as CallHetemlApi from '../utils/CallHetemlApi';
import * as CommonConstant from '../constants/CommonConstant';

function* handleInitData() {yield takeEvery( CommonConstant.INIT, initData );}
function* handleChangeVisibleRegistModal() {yield takeEvery( CommonConstant.CHANGE_VISIBLE_REGIST_MODAL, changeVisibleRegistModal);}
function* handleChangeVisibleUpdateModal() {yield takeEvery( CommonConstant.CHANGE_VISIBLE_UPDATE_MODAL, changeVisibleUpdateModal);}
function* handleChangeVisibleFilterModal() {yield takeEvery( CommonConstant.CHANGE_VISIBLE_FILTER_MODAL, changeVisibleFilterModal);}
function* handleRegistEvent() {yield takeEvery( CommonConstant.REGIST_EVENT, registEvent );}
function* handleUpdateEvent() {yield takeEvery( CommonConstant.UPDATE_EVENT, updateEvent );}
function* handleDeleteEvent() {yield takeEvery( CommonConstant.DELETE_EVENT, deleteEvent );}
function* handleSetFilter() {yield takeEvery( CommonConstant.SET_FILTER, setFilter );}

const WORKER_LIST = [];

function* initData() {
  try {
    yield call(CallGoogleApi.clientLoad);
    yield call(CallGoogleApi.initClient);

    const userId = yield call(authCheck);
    console.log(userId);

    const eventList = [];
    const colorList = [].concat(CommonConstant.COLOR_LIST);

    const orgUserList = yield call(CallHetemlApi.getUsers);

    for(const obj of orgUserList) {
      const user = obj.User;
      WORKER_LIST.push(
        {
          name: user.name,
          address: user.mail_address,
          kana: user.name_kana,
          director: Number(user.director_flag),
          creator: Number(user.creator_flag),
          checker: Number(user.checker_flag),
        }
      )
    }

    console.log(`WORKER_LIST: ${JSON.stringify(WORKER_LIST)}`);

    let index = 0;
    const calendarIdList = [].concat(WORKER_LIST.map(obj => obj.address));
    calendarIdList.push(CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID);

    for (const calendarId of calendarIdList) {

      yield put({
        type: CommonConstant.REDUCER_PROGRESS_INCREMENT,
        progress: (++index / calendarIdList.length) * 100,
      });

      const eventData = yield call(CallGoogleApi.getEventInfo, calendarId, moment().add(-3, 'day'));
      for(const entry of eventData) {
        if (entry.status === 'cancelled') {
          continue;
        }
        const id = `${entry.id}${entry.creator.email}`;
        const tmpCreator = ((calendarId === CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID) ? ((entry.description) ? JSON.parse(entry.description).staff : entry.creator.email) : calendarId).trim();
        // console.log(tmpCreator);
        // console.log(CommonConstant.WORKER_LIST);
        // console.log([].concat(CommonConstant.WORKER_LIST).filter((val) => {
        //   console.log(`"${val.address}"`, `"${tmpCreator}"`, (val.address === tmpCreator));
        //   return (val.address === tmpCreator);
        // })[0]);
        const staff = [].concat(WORKER_LIST).filter((val) => {
          return (val.address === tmpCreator);
        })[0].name;

        const descriptionObj = (calendarId === CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID && entry.description) ? JSON.parse(entry.description) : null;

        const priority = (descriptionObj && descriptionObj.priority) ? descriptionObj.priority : 2;

        const color = (calendarId !== CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID) ? CommonConstant.COLOR_ORIGIN_CALENDAR : CommonConstant.COLOR_RESOUCE_CALENDAR[priority];

        const textColor = (calendarId !== CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID) ? '#000' : '#fff';

        eventList.push({
          orgId: entry.id,
          id,
          title: entry.summary,
          start: entry.start.dateTime || entry.start.date, // try timed. will fall back to all-day
          end: entry.end.dateTime || entry.end.date, // same
          location: entry.location,
          description: entry.description,
          creator: (calendarId === CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID) ? ((entry.description) ? JSON.parse(entry.description).staff : entry.creator.email) : calendarId,
          staff,
          resourceId: id,
          editable: (calendarId === CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID),
          eventColor: color,
          textColor: textColor,
          calendarId,
          memo: (descriptionObj) ? descriptionObj.memo : '',
          priority: (descriptionObj) ? descriptionObj.priority : 1,
        });
      }
    }

    yield put({
      type: CommonConstant.REDUCER_INIT,
      eventList,
      userId,
      workerList: WORKER_LIST,
    });
  } catch(e) {
    console.error(e);
  }
}

function* changeVisibleRegistModal(action) {
  yield put({
    type: CommonConstant.REDUCER_CHANGE_VISIBLE_REGIST_MODAL,
    visible: action.visible,
  })
}

function* changeVisibleUpdateModal(action) {
  yield put({
    type: CommonConstant.REDUCER_CHANGE_VISIBLE_UPDATE_MODAL,
    visible: action.visible,
    event: action.event,
  })
}

function* changeVisibleFilterModal(action) {
  yield put({
    type: CommonConstant.REDUCER_CHANGE_VISIBLE_FILTER_MODAL,
    visible: action.visible,
  })
}

function* registEvent(action) {
  const eventObj = {
    'summary': action.registInfo.title,
    'description': JSON.stringify({
      staff: action.registInfo.staff,
      memo: action.registInfo.memo,
      priority: action.registInfo.priority,
    }),
    'start': {
      'dateTime': action.registInfo.startDate,
      'timeZone': 'Asia/Tokyo'
    },
    'end': {
      'dateTime': action.registInfo.endDate,
      'timeZone': 'Asia/Tokyo'
    },
  };

  yield call(authCheck);

  const registResult = yield call(CallGoogleApi.execInsert, eventObj);
  console.log(JSON.stringify(registResult));

  const id = `${registResult.id}${action.registInfo.staff}`;

  const retObj = {
    orgId: registResult.id,
    id,
    title: registResult.summary,
    start: registResult.start.dateTime,
    end: registResult.end.dateTime,
    location: registResult.location,
    description: registResult.description,
    creator: action.registInfo.staff,
    resourceId: id,
    editable: true,
    eventColor: CommonConstant.COLOR_RESOUCE_CALENDAR[action.registInfo.priority],
    textColor: '#fff',
    calendarId: CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID,
    staff: [].concat(WORKER_LIST).filter((val) => {
      return (val.address === action.registInfo.staff);
    })[0].name,
    memo: action.registInfo.memo,
    priority: action.registInfo.priority,
  };

  yield put({
    type: CommonConstant.REDUCER_REGIST_EVENT,
    result: retObj,
  })
}

function* updateEvent(action) {
  const eventObj = {
    'summary': action.event.title,
    'start': {
      'dateTime': action.event.start,
      'timeZone': 'Asia/Tokyo'
    },
    'end': {
      'dateTime': action.event.end,
      'timeZone': 'Asia/Tokyo'
    },
    'description': action.event.description,
  };

  yield call(authCheck);

  const result = yield call(CallGoogleApi.execUpdate, action.event.id, eventObj);
  console.log(JSON.stringify(result));

  const descriptionObj = JSON.parse(action.event.description);
  const id = `${result.id}${descriptionObj.staff}`;
  const retObj = {
    orgId: result.id,
    title: result.summary,
    start: result.start.dateTime,
    end: result.end.dateTime,
    location: result.location,
    description: result.description,
    creator: descriptionObj.staff,
    editable: true,
    eventColor: CommonConstant.COLOR_RESOUCE_CALENDAR[descriptionObj.priority],
    textColor: '#fff',
    calendarId: CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID,
    staff: [].concat(WORKER_LIST).filter((val) => {
      return (val.address === descriptionObj.staff);
    })[0].name,
    memo: descriptionObj.memo,
    priority: descriptionObj.priority,
  };

  yield put({
    type: CommonConstant.REDUCER_UPDATE_EVENT,
    result: retObj,
  });
}

function* deleteEvent(action) {
  const result = yield call(CallGoogleApi.execDelete, action.eventId);
  console.log(JSON.stringify(result));

  yield call(authCheck);

  yield put({
    type: CommonConstant.REDUCER_DELETE_EVENT,
    eventId: action.eventId,
  });
}

function* setFilter(action) {
  yield put({
    type: CommonConstant.REDUCER_SET_FILTER,
    list: action.list,
  })
}

function* authCheck() {
  let isAuthorized = null;
  const GoogleAuth = gapi.auth2.getAuthInstance();

  const user = GoogleAuth.currentUser.get();
  isAuthorized = user.hasGrantedScopes(CommonConstant.GOOGLE_API_SCOPES);

  if (!isAuthorized) {
    GoogleAuth.signIn();
  }

  return user.getBasicProfile().getEmail();
}

function updateSigninStatus(isSignedIn) {
  setSigninStatus();
}

const commonSaga = [
  fork(handleInitData),
  fork(handleChangeVisibleRegistModal),
  fork(handleChangeVisibleUpdateModal),
  fork(handleChangeVisibleFilterModal),
  fork(handleRegistEvent),
  fork(handleUpdateEvent),
  fork(handleDeleteEvent),
  fork(handleSetFilter),
];

export default commonSaga;
