import { combineReducers } from 'redux';
import common from './reducers/CommonReducer';

const rootReducer = combineReducers({
  common,
});

export default rootReducer;
