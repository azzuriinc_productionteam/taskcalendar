import axios from 'axios';

import { delay } from 'redux-saga';
import { all } from 'redux-saga/effects';

import commonSaga from './sagas/CommonSaga';

export default function* rootSaga() {
  yield all([
    ...commonSaga,
  ]);
}
