import axios from 'axios';
import * as CommonConstant from '../constants/CommonConstant';

export function getUsers() {
  console.log(`url: ${CommonConstant.HETEML_API_URL_USER}`);
  const headers = {
    'content-type': 'application/json',
  }
  return axios({
    method: 'get',
    url: CommonConstant.HETEML_API_URL_USER,
    headers: headers,
    auth: {
      username: CommonConstant.API_BASIC_AUTH_USERNAME,
      password: CommonConstant.API_BASIC_AUTH_PASSWORD,
    }
  })
  .then( res => {
    const result = res;
    console.log(JSON.stringify(result));
    return result.data.users;
  })
  .catch( err => {
    return err;
  });
}
