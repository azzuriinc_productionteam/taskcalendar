import * as CommonConstant from '../constants/CommonConstant';

export function clientLoad() {
  return new Promise(
    (resolve, reject) => {
      gapi.load('client:auth2', () => {resolve()});
    }
  );
}

export function initClient() {
  return gapi.client.init({
      'apiKey': '',
      'discoveryDocs': [CommonConstant.GOOGLE_API_DISCOVERY_URL],
      'clientId': CommonConstant.GOOGLE_API_CLIENT_ID,
      'scope': CommonConstant.GOOGLE_API_SCOPES,
    });
}

export function getEventInfo(calendarId, timeMin) {
  return new Promise(
    (resolve, reject) => {
      gapi.client.load('calendar', 'v3').then(
        () => {
          const req = gapi.client.calendar.events.list({
            calendarId,
            timeMin: timeMin.format(),
          }).then((res) => {
            if (res.result.error) {
              reject(res);
            } else {
              if (res.result.summary === 'リソース管理') {
                resolve(res.result.items);
              } else {
                resolve(res.result.items.map(val => Object.assign({}, val, {creator: {email: res.result.summary}})))
              }
            }
          })
        }
      )
    }
  )
}

export function execInsert(eventObj) {
  const request = gapi.client.calendar.events.insert({
    'calendarId': CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID,
    'resource': eventObj,
  });
  return new Promise(
    (resolve, reject) => {
      request.execute(event => {resolve(event.result)});
    }
  )
}

export function execUpdate(eventId, resource) {
  const request = gapi.client.calendar.events.update({
    'calendarId': CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID,
    eventId,
    resource,
  });
  return new Promise(
    (resolve, reject) => {
      request.execute(event => {resolve(event.result)});
    }
  )
}

export function execDelete(eventId) {
  const request = gapi.client.calendar.events.delete({
    'calendarId': CommonConstant.GOOGLE_RESOURCE_CALENDAR_ID,
    eventId,
  });
  return new Promise(
    (resolve, reject) => {
      request.execute(event => {resolve(event.result)});
    }
  )
}
